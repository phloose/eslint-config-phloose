## eslint-config-phloose

This is my personal [ESLint](https://eslint.org) config that i've used over time in
different projects. The base config extends `airbnb-base` and `eslint:recommended`
configurations. Further the package contains configs for `node`, `jest`, `vue`,
`typescript` and `svelte`.

### Install

This configuration assumes that ([ESLint](https://eslint.org) is installed.

Peer dependencies:

```bash
npm i -D eslint
```

Config installation:

```bash
npm i -D eslint-config-phloose
```

### Usage

To use it simply add it to the `extends` property in a projects .eslintrc-file:

```javascript
module.exports = {
    extends: ["phloose"]
    rules: {
        /* settings... */
    }
}
```

The specialized configs can be referenced in the way `phloose/<specialized>` where
`specialized` is one of `jest`, `node`, `svelte`, `ts` or `vue`

All specialized configurations extend the [base](base.js) config so it does not need to
be listed when using one of them.

### Recommended usage

For the best user experience regarding formatting and linting it is best to use
[prettier-eslint](https://github.com/prettier/prettier-eslint), which will run
[Prettier](https://github.com/prettier/prettier) first and then
[ESLint](https://eslint.org).

Previously my workaround was to use [Prettier](https://github.com/prettier/prettier)  as
an [ESLint](https://eslint.org) plugin so that prettier errors would show up when
running [ESLint] (https://eslint.org) and also be fixed by running
[ESLint](https://eslint.org) either from VSCode or from the command line. It has been
shown that this approach has its limits and i came to the conclusion that it is better
to have one tool for a specific job:

- Prettier just for formatting
- ESLint for highlighting and fixing code style issues

To be in line with this configuration a .prettierrc-file with the following settings
needs to be added to the root folder of a project (example uses .js-format):

```javascript
module.exports = {
    arrowParens: "avoid",
    endOfLine: "auto",
    tabWidth: 4,
    trailingComma: "all",
    quoteProps: "consistent",
};
```

For the following [Prettier](https://github.com/prettier/prettier) needs to be installed:

```bash
npm i -D prettier
```

#### Use prettier-eslint from the command line

To use [prettier-eslint](https://github.com/prettier/prettier-eslint) with this
configuration from the commandline run:

```bash
npm i -D prettier-eslint-cli eslint-prettier-config
```

which will install the CLI for
[prettier-eslint](https://github.com/prettier/prettier-eslint) and a prettier specific
[ESLint](https://eslint.org) config that disables certain conflicting
[ESLint](https://eslint.org) rules.

Then add prettier to the end of the `extends` array in the .eslintrc-file (this is
important!):

```javascript
module.exports = {
    extends: ["phloose", "prettier"]
    rules: {
        /* additional rules */
    }
}
```

To run [prettier-eslint](https://github.com/prettier/prettier-eslint) e.g. on all `js`
and `ts` files of the `src` folder and subfolders issue the following:

```bash
prettier-eslint --write "src/**/*.js"
```

#### Use Prettier ESLint VSCode extension

Install the [prettier-eslint](https://github.com/prettier/prettier-eslint) core package
and the config mentioned already above:

```bash
npm i -D prettier-eslint eslint-prettier-config
```

Then install [Prettier
ESLint](https://marketplace.visualstudio.com/items?itemName=rvest.vs-code-prettier-eslint)
extension for VSCode and add a workspace configuration with the following content:

```json
{
    "[<language>]": {
        "editor.defaultFormatter": "rvest.vs-code-prettier-eslint"
    },
    "editor.codeActionsOnSave": {
        "source.fixAll": true
    },
    "eslint.validate": [
        "<language>"
    ],
}
```

This way VSCode formats with Prettier and also hightlights and fixes ESLint errors for
the respective filetype/language set in brackets.

The .eslintrc-file has to be configured like in the previous section.
