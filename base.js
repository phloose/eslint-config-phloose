module.exports = {
    env: {
        browser: true,
        es6: true,
        jest: true,
        node: true,
    },
    extends: ["airbnb-base", "eslint:recommended"],
    overrides: [
        {
            files: ["**/*.spec.js"],
        },
    ],
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module",
    },
    /** Rules should be sorted alphabetically */
    rules: {
        "arrow-parens": ["error", "as-needed"],
        "func-names": "off",
        "import/no-extraneous-dependencies": [
            "error",
            {
                devDependencies: true,
            },
        ],
        "indent": [
            "error",
            4,
            {
                SwitchCase: 1,
            },
        ],
        "linebreak-style": "off",
        "no-await-in-loop": "error",
        "no-console": [
            "error",
            {
                allow: ["warn", "error"],
            },
        ],
        "no-param-reassign": [
            "error",
            {
                props: false,
            },
        ],
        "no-plusplus": [
            "error",
            {
                allowForLoopAfterthoughts: true,
            },
        ],
        "no-underscore-dangle": [
            "error",
            {
                allowAfterThis: true,
            },
        ],
        "no-unused-expressions": [
            "error",
            {
                allowShortCircuit: true,
            },
        ],
        "no-unused-vars": [
            "error",
            {
                argsIgnorePattern: "rest|^_|_",
                ignoreRestSiblings: true,
            },
        ],
        "quote-props": ["error", "consistent-as-needed"],
        "quotes": [
            "error",
            "double",
            {
                avoidEscape: true,
            },
        ],
    },
};
