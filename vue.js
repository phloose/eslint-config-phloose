module.exports = {
    extends: ["./base.js", "plugin:vue/recommended"],
    plugins: ["vue"],
    overrides: [
        {
            files: ["**/*.vue"],
        },
    ],
    rules: {
        "vue/html-indent": ["error", 4],
    },
};
