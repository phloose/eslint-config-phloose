module.exports = {
    extends: ["./base.js", "plugin:node/recommended"],
    plugins: ["node"],
    rules: {
        "global-require": "error",
        "handle-callback-err": "error",
        "no-buffer-constructor": "error",
        "no-mixed-requires": "error",
        "no-new-require": "error",
        "no-path-concat": "error",
        "no-process-exit": "error",
    },
};
