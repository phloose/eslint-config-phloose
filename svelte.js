module.exports = {
    extends: ["./base.js"],
    plugins: ["svelte3"],
    overrides: [
        {
            files: ["**/*.svelte"],
            processor: "svelte3/svelte3",
            rules: {
                "import/first": "off",
                "import/no-duplicates": "off",
                "import/no-mutable-exports": "off",
                "import/no-unresolved": "off",
            },
        },
    ],
};
